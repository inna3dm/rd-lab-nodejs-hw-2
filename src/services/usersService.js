const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserProfile = async (userId) => {
  const user = await User.findOne({_id: userId});
  return user;
};

const updateUserProfile = async (userId, data) => {
  const user = await User.findOne({_id: userId});

  if (data.password) {
    data.password = await bcrypt.hash(data.password, 10);
  }

  await User.updateOne(user, {$set: {data}});
};

const deleteUserProfile = async (userId) => {
  await User.findOneAndRemove({_id: userId});
};

module.exports = {
  getUserProfile,
  updateUserProfile,
  deleteUserProfile,
};
