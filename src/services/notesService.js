const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId, query) => {
  const offset = query.offset || 0;
  const limit = query.limit || 5;
  const allNotes = await Note.find({userId});
  const count = allNotes.length;

  const notes = await Note.find({userId})
      .select('-password')
      .skip(parseInt(offset))
      .limit(parseInt(limit));

  return {offset, limit, count, notes};
};

const addNoteToUser = async (data, userId) => {
  const note = new Note({...data, userId});
  await note.save();
};

const getNoteByIdForUser = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId});
  return note;
};

const updateNoteByIdForUser = async (noteId, userId, data) => {
  await Note.findOneAndUpdate({_id: noteId, userId}, {$set: data});
};

const toggleCompletedInNoteByIdForUser = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId});
  const state = !note.completed;

  await Note.updateOne(note, {completed: state});
  return state;
};

const deleteNoteByIdForUser = async (noteId, userId) => {
  await Note.findOneAndRemove({_id: noteId, userId});
};


module.exports = {
  getNotesByUserId,
  addNoteToUser,
  getNoteByIdForUser,
  updateNoteByIdForUser,
  deleteNoteByIdForUser,
  toggleCompletedInNoteByIdForUser,
};
