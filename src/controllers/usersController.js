const express = require('express');
const router = express.Router();

const {
  getUserProfile,
  updateUserProfile,
  deleteUserProfile,
} = require('../services/usersService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const user = await getUserProfile(userId);

  if (!user) {
    throw new Error('No user found!');
  }

  res.json({user});
}));

router.patch('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await updateUserProfile(userId, req.body);

  res.json({message: 'Your profile has been updated successfully'});
}));

router.delete('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await deleteUserProfile(userId);
  res.json({message: 'Your profile has been deleted'});
}));

module.exports = {
  usersRouter: router,
};
