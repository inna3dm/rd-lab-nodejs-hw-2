const express = require('express');
const router = express.Router();

const {
  registration,
  login,
} = require('../services/authService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.post('/register', asyncWrapper(async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  await registration({username, password});

  res.json({message: 'Account created successfully'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  const token = await login({username, password});

  res.json({message: 'Logged in successfully', jwt_token: token});
}));

module.exports = {
  authRouter: router,
};
