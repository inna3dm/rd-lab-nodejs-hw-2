const express = require('express');
const router = express.Router();

const {
  getNotesByUserId,
  addNoteToUser,
  getNoteByIdForUser,
  updateNoteByIdForUser,
  toggleCompletedInNoteByIdForUser,
  deleteNoteByIdForUser,
} = require('../services/notesService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const notes = await getNotesByUserId(userId, req.query);

  res.json(notes);
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  const note = await getNoteByIdForUser(id, userId);

  if (!note) {
    throw new Error('No note with such id found!');
  }
  res.json({note});
}));

router.post('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await addNoteToUser(req.body, userId);

  res.json({message: 'Note created successfully'});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await updateNoteByIdForUser(id, userId, req.body);

  res.json({message: 'Note updated successfully'});
}));

router.patch('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  const state = await toggleCompletedInNoteByIdForUser(id, userId);
  const msg = state ? 'Note completed' : 'Note uncompleted';

  res.json({message: msg});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await deleteNoteByIdForUser(id, userId);
  res.json({message: 'Note deleted'});
}));

module.exports = {
  notesRouter: router,
};
